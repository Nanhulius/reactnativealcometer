import React from 'react';
import { View } from 'react-native';
import Alcometer from './Alcometer';
import styles from './styles/Style';

export default function App() {
  return (
    <View style= {styles.container}>
      <Alcometer />
    </View>
  );
}