import React, { useState } from 'react'
import { Text, TextInput, View, Button} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Picker } from '@react-native-picker/picker';
import styles from './styles/Style';

export default function Alcometer() {

    const [gender, setGender] = useState(0);
    const [weight, setWeight] = useState(0);
    const [bottle, setBottles] = useState(0);
    const [time, setTime] = useState(0);
    const [result, setResult] = useState(0);

    const bottles = [
        { label: '1 bottle', value: 1 },
        { label: '2 bottles', value: 2 },
        { label: '3 bottles', value: 3 },
        { label: '4 bottles', value: 4 },
        { label: '5 bottles', value: 5 },
        { label: '6 bottles', value: 6 },
        { label: '7 bottles', value: 7 },
        { label: '8 bottles', value: 8 },
        { label: '9 bottles', value: 9 },
        { label: '10 bottles', value: 10 },
        { label: '11 bottles', value: 11 },
        { label: 'Whole damn mäyris', value: 12 }
    ];

    const hours = [
        { label: '1 hour', value: 1 },
        { label: '2 hours', value: 2 },
        { label: '3 hours', value: 3 },
        { label: '4 hours', value: 4 },
        { label: '5 hours', value: 5 },
        { label: '6 hours', value: 6 },
        { label: '7 hours', value: 7 },
        { label: '8 hours', value: 8 },
        { label: '9 hours', value: 9 },
        { label: '10 hours', value: 10 },
        { label: '11 hours', value: 11 },
        { label: 'All day tissuttelua', value: 12 },
    ]

    const genders = [
        { label: 'Male   ', value: 1 },
        { label: 'Female   ', value: 2 },
        { label: 'Other', value: 3}
    ];

    function calculate() {
        let litres = Math.round(bottle * 0.33).toFixed(2);
        let grams = Math.round(litres * 8 * 4.5).toFixed(2);
        let burning = Math.round(weight / 10).toFixed(2);
        let gramsLeft = Math.round(grams - burning * time).toFixed(2);
        let result = 0;
        if (gender === 1) {
            result = gramsLeft / (weight * 0.7);
        }
        else if (gender === 2) {
            result = gramsLeft / (weight * 0.6);
        }
        else if (gender === 3) {
            result = gramsLeft / (weight * 0.65);
        }
       
        if (result < 0) {
            result = 0;
        }
        setResult(result);
    }

    return (
        <>
            <View style={styles.container}>
                <View>
                    <Text style={styles.header}>Alcometer</Text>
                </View>
                <View style={styles.borderBox}>
                    <View style={styles.border}></View>
                </View>
                <View>
                    <Text style={styles.text}>Choose your gender: </Text>
                </View>
                <RadioForm style={styles.radio}
                    radio_props={genders}
                    initial={-1}
                    onPress={(value) => { setGender(value) }}
                    formHorizontal={true}               
                />
                <View style={styles.borderBox}>
                    <View style={styles.border}></View>
                </View>
                <View style={styles.textLabel}>
                    <Text style={styles.text}>Insert your weight: </Text>
                        <TextInput style={styles.input}
                        onChangeText={text => setWeight(text)}
                        keyboardType='numeric'
                        placeholder=' here  '></TextInput>
                    <Text style={styles.text}>kg</Text>
                </View>
                <View style={styles.borderBox}>
                    <View style={styles.border}></View>
                </View>
                <View>
                    <Text style={styles.text}>How many bottles (0,33l) you had: </Text>
                        <Picker style={styles.list}
                            selectedValue={bottle}
                            onValueChange={(value) => setBottles(value)}>
                                {bottles.map((bottle, value) => (
                                    <Picker.Item key={value} label={bottle.label} value={bottle.value} />
                                ))
                                }
                        </Picker>
                </View>
                <View style={styles.borderBox}>
                    <View style={styles.border}></View>
                </View>
                <View>
                    <Text style={styles.text}>In how many hours you drank them: </Text>
                        <Picker style={styles.list}
                            selectedValue={time}
                            onValueChange={(value) => setTime(value)}>
                                {hours.map((time, index) => (
                                    <Picker.Item key={index} label={time.label} value={time.value} />
                                ))
                                }
                        </Picker>
                </View>
                <View style={styles.borderBox}>
                    <View style={styles.border}></View>
                </View>
                <View>
                    <Button onPress={calculate} title="Calculate" />
                </View>
                <View style={styles.borderBox}>
                    <View style={styles.border}></View>
                </View>
                <View style={styles.textLabel}>
                    <Text style={styles.result}>Your promilles are: {result.toFixed(2)}</Text>
                </View>
            </View>
           
        </>
    )
}
