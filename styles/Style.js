import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    radio: {
      marginTop: 10,
      marginRight: 10,
      marginLeft: 10,
      marginBottom: 10,
    },
    textLabel: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    header: {
      fontSize: 50,
    },
    text: {
      fontWeight: 'bold',
    },
    input: {
      textAlign: 'center'
    },
    list: {
      marginTop: 15,
      textAlign: 'center',
      fontWeight: 'bold', 
    },
    borderBox: {
      flexDirection: 'row',
      alignItems: 'center',
      height: 1,
      margin: 10,
    },
    border: {
      flex: 1,
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: 'black',
    },
    result: {
      fontWeight: 'bold',
      fontSize: 30,
    }
  });
  